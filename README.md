## <div align="center">Hey 👋, I'm Sébastien Cailleau!</div>  
  

<div align="center">
<a href="https://twitter.com/@MrToorop" target="_blank">
<img src=https://img.shields.io/badge/twitter-%2300acee.svg?&style=for-the-badge&logo=twitter&logoColor=white alt=twitter style="margin-bottom: 5px;" />
</a>
<a href="https://linkedin.com/in/sébastien-cailleau" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a>  
</div>  
  



### Glad to see you here!  
I am 40, live in France, curiosity and eagerness to learn are my engines to gains skills  
  

<br/>  

<table><tr><td valign="top" width="50%">

- 🔭 I’m currently working on **Python** with *FastApi* and **C#** with *Dotnet*
  

- ❓ Ask me about anything related to **Python, FastApi and related technologies**  
  

- ⚡ I’m looking to collaborate on any **open source project**  
  

- *📬 How to reach me sebastien.cailleau@protonmail.com*  


</td><td valign="top" width="50%">

<div align="center">
<img src="https://rishavanand.github.io/static/images/greetings.gif" align="center" style="width: 100%" />
</div>  


</td></tr></table>  

<br/>  


## Languages and Tools  
<div align="center"> 
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/mysql-original-wordmark.svg" alt="MySQL" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/python-original.svg" alt="Python" height="50" />
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/gnu_bash-icon.svg" alt="Bash" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/linux-original.svg" alt="Linux" height="50" />  
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/git-scm-icon.svg" alt="Git" height="50" />    
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/bootstrap-plain.svg" alt="Bootstrap" height="50" />
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/javascript-original.svg" alt="JavaScript" height="50" />
<img style="margin: 10px" src="https://profilinator.rishav.dev/skills-assets/php-original.svg" alt="PHP" height="50" /> 

</div>  

<br/>  


----
<div align="center">Generated using <a href="https://profilinator.rishav.dev/" target="_blank">Github Profilinator</a></div>
